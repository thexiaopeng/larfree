<?php

namespace Larfree\Console\Commands;

use App\Models\Admin\AdminNav;
use App\Models\Common\CommonUser;
use Illuminate\Console\Command;
use Larfree\Libs\Make;

class LarfreeInstall extends Command
{
    protected $signature = 'larfree:install {replace=y}';

    /**
     * The console command description.
     *
     * @var string
     **/
    protected $description = '初始化数据库和其他配置';

    /**
     * Create a new command instance.
     *
     * @return void
     **/
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arguments = $this->arguments();
        $replace = $arguments['replace'];
        $this->createAdmin();
        $this->createNav();
    }

    private function createNav(){
        $nav=[
            'name'=>'测试',
            'url'=>'/curd/test.test/',
            'class'=>'',
            'module'=>'',
            'status'=>1,
        ];
        CommonUser::create($nav);
        $nav=[
            'name'=>'用户管理',
            'url'=>'/curd/common.user/',
            'class'=>'',
            'module'=>'',
            'status'=>1,
        ];
        CommonUser::create($nav);
    }

    private function createAdmin(){
        CommonUser::create(['name'=>'admin',
            'phone'=>'15680206876',
            'mail'=>'534257342@qq.com',
            'api_token'=>str_random(30),
            'password'=>'www78546',
        ]);
    }

}
